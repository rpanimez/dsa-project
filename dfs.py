from binarytree import biTree
"""
Line 1 is data
Line 2 is what to search?
"""
inputLine1 = [int(x) for x in input().split(',')]
inputLine2 = [int(x) for x in input().split(',')]
bt = biTree()

"""
return the index the element were found
increment index for each node visited
-1 if not found
"""
def dfs(biTree,e,p=None,c=0):
    if biTree.root == None:
        return -1
    if p == None:
        p = biTree.root
    if p._data == e:
        return c
    if e < p._data:
        if p._left == None:
            return -1
        return dfs(biTree,e,p._left,c+1)
    if p._right == None:
        return -1
    return dfs(biTree,e,p._right,c+1)

for d in inputLine1:
    bt.insert(d)

for e in inputLine2:
    print(str(dfs(bt,e)))