class Node:
        __slots__ = '_data','_parent','_left','_right'
        def __init__(self,data,parent = None,left=None,right=None):
            self._data = data
            self._parent = parent
            self._left = left
            self._right = right

class biTree():
    __slots__ = 'root'
    def __init__(self):
        self.root = None
    """
    insert node instead of element
    """
    def _insertNode(self,p,e):
        done = False
        while not done:
            """
            No duplicate
            If duplicate -> ignore
            """
            if e == None:
                """
                Make sure Node is not NIL
                """
                done = True
            else:
                if p._data > e._data:
                    if p._left == None:
                        p._left = e
                        e._parent = p
                        done = True
                    else:
                        p = p._left
                elif p._data < e._data:
                    if p._right == None:
                        p._right = e
                        e._parent = p
                        done = True
                    else:
                        p = p._right
                else:
                    """
                    Duplicate found
                    Not < && Not >
                    """
                    done = True
    """
    create a node && insert element by checking it values then branch accordingly
    """
    def insert(self,e):
        if self.root == None:
            self.root = Node(e)
        else:
            done = False
            w = self.root
            while not done:
                """
                No duplicate
                If duplicate -> ignore
                """
                if w._data > e:
                    if w._left == None:
                        wl = Node(e)
                        w._left = wl
                        wl._parent = w
                        done = True
                    else:
                        w = w._left
                elif w._data < e:
                    if w._right == None:
                        wr = Node(e)
                        w._right = wr
                        wr._parent = w
                        done = True
                    else:
                        w = w._right
                else:
                    """
                    Duplicate found
                    Not < && Not >
                    """
                    done = True
   
    """ find is implemented with depth first search """
    def find(self,e,p=None):
        if self.root == None:
            return None
        if p == None:
            p = self.root
        if p._data == e:
            return p
        if e < p._data:
            if p._left == None:
                return None
            return self.find(e,p._left)
        if p._right == None:
            return None
        return self.find(e,p._right)

    def delete(self,e):
        p = self.find(e)
        if p == None:
            return None
        a = p._parent
        x = p._left
        y = p._right
        """ 
        To delete, we must reconnect the rest of the node
        In doing so, branches consider the difference between
        NodeToBeDeleted.parent and its children
        We take the child closest to the paren
        to replace the deleted node

        If the deleted node is to the left of parent
            we look at it's right (closest to parent)
        If the deleted node is to the right of parent
            we look at it's left (closest to parent)
        """
        if a == None:
            """ a is None meaning P is the root"""
            if (p._data - x._data) > (y._data - p._data):
                y = self.root
                self._insertNode(y,x)
                return None
            x = self.root
            self._insertNode(x,y)
            return None
        if p == a._left:
            if p._right != None:
                a._left = y
                y._parent = a
                self._insertNode(y,x)
            else:
                a._left = x
                x._parent = a
                self._insertNode(x,y)
        else:
            if p._left != None:
                a._right = x
                x._parent = a
                self._insertNode(x,y)
            else:
                a._right = y
                y._parent = a
                self._insertNode(y,x)
        p = None
    