from binarytree import biTree
"""
Line 1 is data
Line 2 is what to search?
"""
inputLine1 = [int(x) for x in input().split(',')]
inputLine2 = [int(x) for x in input().split(',')]
bt = biTree()

"""
return the index the element were found
increment index for each node visited
-1 if not found
"""
def bfs(biTree,e):
    """
    Tree is empty
    """
    if biTree.root == None:
        return -1
    if biTree.root._data == e:
        return 1
    p = biTree.root
    """
    c counts the number of node visited
    """
    c = 0
    done = False
    l = list()
    """
    using list instead of queue
    """
    while not done:
        if p._left!=None:
            c += 1
            if p._left._data == e:
                done = True
                return c
            l.append(p._left)
        if p._right!=None:
            c += 1
            if p._right._data == e:
                done = True
                return c
            l.append(p._right)
        if len(l) == 0:
            return -1
        p = l[0]
        del l[0]

for d in inputLine1:
    bt.insert(d)

for e in inputLine2:
    print(str(bfs(bt,e))+)